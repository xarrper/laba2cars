
public class SalableCar extends SalableVehicle {

	private Car car;
	private String make;
	private String model;
	private String color;
	private int year;
	
	public SalableCar(String vin, int mileage, int price, String make, String model, String color, int year) {
		super(vin, mileage, price);
		car = new Car();
		this.make = make;
		this.model = model;
		this.color = color;
		this.year = year;
	}

	public String getMakeAndModel() {
		return car.getModel()+car.getModel();
	}

	@Override
	public String getColor() {
		return car.getColor();
	}

	@Override
	public int getYear() {
		return car.getYear();
	}

	@Override
	public String toString() {
		return "SalableCar [car=" + car + "]";
	}

}
